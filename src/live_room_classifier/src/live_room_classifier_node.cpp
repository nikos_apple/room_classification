#include <ros/ros.h>
#include <iostream>
#include <fstream>
#include <iterator>
#include <vector>
#include "sensor_msgs/LaserScan.h"
#include <dlib/svm_threaded.h>
#include <sstream>
#include <string>
#include <fstream>
#include <dlib/matrix.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <std_msgs/Int32.h>

class live_classifier
{
public:
  ros::NodeHandle nh;
  ros::Subscriber sensor_sub;

  typedef dlib::matrix<float, 720, 1> one_scan;
  one_scan one_scan_data;
  std::vector<one_scan> all_scans;
    

  live_classifier()
  {
    sensor_sub = nh.subscribe("/scan", 1000, &live_classifier::sensorCallback, this);
  }


  void sensorCallback(const sensor_msgs::LaserScan::ConstPtr &msg)
  {
    all_scans.clear();

    int c = 0;

    for (size_t idx = 0; idx < msg->ranges.size(); idx++)
    {
      
      if (msg->ranges[idx] < msg->range_min)
      {
        if(c<=6){
          if (idx == 0){
            // distances.push_back(msg->range_max);
            one_scan_data(idx, 0) = msg->range_max;
            c = c + 1;
          }
          else{
             one_scan_data(idx, 0) = msg->ranges[idx-1];
            c = c + 1;
          }
        }
        else if(c>6){
           one_scan_data(idx, 0) = msg->range_max;
        }

      }
      else
      {
        one_scan_data(idx, 0) = msg->ranges[idx];
        c = 0;
      }
    }
    all_scans.push_back(one_scan_data);

    typedef dlib::linear_kernel<one_scan> lin_kernel;
    dlib::multiclass_linear_decision_function<lin_kernel, float> df;
    dlib::deserialize("saved_function.dat") >> df;
    int room_class_no = df(one_scan_data);
    if (room_class_no == 0){
      std::cout << "Room: \n" << "Corridor" << std::endl;
    }
    else if (room_class_no == 1){
      std::cout << "Room: \n" << "Lab" << std::endl;
    }
    // std::string room = 
    // std::cout << "Room: \n" << df(one_scan_data) << std::endl;
  
    // ROS_INFO_STREAM( all_scans.size());
  } 
  bool ok()
  {
    return nh.ok();
  }
  
};


int main(int argc, char *argv[])
{
  // Init the connection with the ROS system
  ros::init(argc, argv, "live_classifier");

  // Create class instances
  live_classifier live_cl;

  ros::Rate loop_rate(10);
  int i = 0;
  while (live_cl.ok())
  {
    ros::spinOnce();
    loop_rate.sleep();
  }
  return 0;
}