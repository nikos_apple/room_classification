#include <ros/ros.h>
#include <iostream>
#include <fstream>
#include <iterator>
#include <vector>
#include "sensor_msgs/LaserScan.h"
#include <dlib/svm_threaded.h>
#include <sstream>
#include <string>
#include <fstream>
#include <dlib/matrix.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <std_msgs/Int32.h>

void populate_data(sensor_msgs::LaserScan::ConstPtr msg, int flag);

typedef dlib::matrix<float, 720, 1> one_scan;
one_scan one_scan_data;
std::vector<one_scan> all_scans;
std::vector<one_scan> val_set;

std::vector<float> labels;
std::vector<float> val_labels;


int main(int argc, char **argv)
{
  
  ros::init(argc, argv, "room_classifier_train");
  ros::NodeHandle nh;

  ros::Rate loop_rate(10);

  rosbag::Bag bag;

  // ///////////////////////////// 1st bag read above cellar//////////////////////////////////////////////
  // std::string source_bag = "/home/nikos/ml_proj/scan_and_bag_files/training_set_bag_files/training/above_cellar/drive_2019-03-09-10-43-56.bag";
  // bag.open(source_bag);  // BagMode is Read by default

  // for(rosbag::MessageInstance const m: rosbag::View(bag))
  // {
  //   sensor_msgs::LaserScan::ConstPtr msg = m.instantiate<sensor_msgs::LaserScan>();
  //   if (msg != NULL){
  //     populate_data(msg,0);
  //   }
  // }
  // float above_cellar = all_scans.size();
  // ROS_INFO_STREAM("above cellar: " << above_cellar);

  // bag.close();

  // ///////////////////////////// 2nd bag read cellar//////////////////////////////////////////////
  // source_bag = "/home/nikos/ml_proj/scan_and_bag_files/training_set_bag_files/training/cellar/drive_2019-03-02-13-43-36.bag";
  // bag.open(source_bag);  // BagMode is Read by default

  // for(rosbag::MessageInstance const m: rosbag::View(bag))
  // {
  //   sensor_msgs::LaserScan::ConstPtr msg = m.instantiate<sensor_msgs::LaserScan>();
  //   if (msg != NULL){
  //     populate_data(msg,0);
  //   }
  // }
  // float cellar_1 = all_scans.size() - above_cellar;
  // ROS_INFO_STREAM("cellar_1: " << cellar_1);
  
  // // ROS_INFO_STREAM(all_scans.size());

  // bag.close();

  // ///////////////////////////// 3rd bag read cellar//////////////////////////////////////////////
  // source_bag = "/home/nikos/ml_proj/scan_and_bag_files/training_set_bag_files/training/cellar/drive_2019-03-02-13-48-51.bag";
  // bag.open(source_bag);  // BagMode is Read by default

  // for(rosbag::MessageInstance const m: rosbag::View(bag))
  // {
  //   sensor_msgs::LaserScan::ConstPtr msg = m.instantiate<sensor_msgs::LaserScan>();
  //   if (msg != NULL){
  //     populate_data(msg,0);
  //   }
  // }
  // float cellar_2 = all_scans.size() - (above_cellar + cellar_1);
  // ROS_INFO_STREAM("cellar_2: " << cellar_2);
  
  // // ROS_INFO_STREAM(all_scans.size());

  // bag.close();

  // ///////////////////////////// 4th bag read cellar//////////////////////////////////////////////
  // source_bag = "/home/nikos/ml_proj/scan_and_bag_files/training_set_bag_files/training/cellar/drive_2019-03-02-13-26-02.bag";
  // bag.open(source_bag);  // BagMode is Read by default

  // for(rosbag::MessageInstance const m: rosbag::View(bag))
  // {
  //   sensor_msgs::LaserScan::ConstPtr msg = m.instantiate<sensor_msgs::LaserScan>();
  //   if (msg != NULL){
  //     populate_data(msg,0);
  //   }
  // }
  // float cellar_3 = all_scans.size() - (above_cellar + cellar_1 + cellar_2);
  // ROS_INFO_STREAM("cellar_3: " << cellar_3);
  
  // // ROS_INFO_STREAM(all_scans.size());

  // bag.close();
  

  ///////////////////////////// 5th bag read corridor//////////////////////////////////////////////
  std::string source_bag = "/home/nikos/ml_proj/scan_and_bag_files/training_set_bag_files/training/corridor/drive_2019-03-02-13-16-49.bag";
  bag.open(source_bag);  // BagMode is Read by default

  for(rosbag::MessageInstance const m: rosbag::View(bag))
  {
    sensor_msgs::LaserScan::ConstPtr msg = m.instantiate<sensor_msgs::LaserScan>();
    if (msg != NULL){
      populate_data(msg,0);
    }
  }

  // float corridor = all_scans.size() - (above_cellar + cellar_1 + cellar_2 + cellar_3);
  float corridor = all_scans.size();
  ROS_INFO_STREAM("corridor: " << corridor);

  bag.close();

  ///////////////////////////// 6th bag read corridor upper//////////////////////////////////////////////
  source_bag = "/home/nikos/ml_proj/scan_and_bag_files/training_set_bag_files/training/corridor_upper/drive_2019-03-09-10-45-24.bag";
  bag.open(source_bag);  // BagMode is Read by default

  for(rosbag::MessageInstance const m: rosbag::View(bag))
  {
    sensor_msgs::LaserScan::ConstPtr msg = m.instantiate<sensor_msgs::LaserScan>();
    if (msg != NULL){
      populate_data(msg,0);
    }
  }

  // float corridor_upper = all_scans.size() - (above_cellar + cellar_1 + cellar_2 + cellar_3 + corridor);
  float corridor_upper = all_scans.size() - corridor;
  ROS_INFO_STREAM("corridor_upper: " << corridor_upper);

  bag.close();

  ///////////////////////////// 7th bag read corridor opposite//////////////////////////////////////////////
  source_bag = "/home/nikos/ml_proj/scan_and_bag_files/training_set_bag_files/training/opposite_corridor/drive_2019-03-09-13-44-17.bag";
  bag.open(source_bag);  // BagMode is Read by default

  for(rosbag::MessageInstance const m: rosbag::View(bag))
  {
    sensor_msgs::LaserScan::ConstPtr msg = m.instantiate<sensor_msgs::LaserScan>();
    if (msg != NULL){
      populate_data(msg,0);
    }
  }

  // float opposite_corridor = all_scans.size() - (above_cellar + cellar_1 + cellar_2 + cellar_3 + corridor + corridor_upper);
  float opposite_corridor = all_scans.size() - (corridor + corridor_upper);
  ROS_INFO_STREAM("opposite_corridor: " << opposite_corridor);

  bag.close();

  ///////////////////////////// 8th bag read t002//////////////////////////////////////////////
  source_bag = "/home/nikos/ml_proj/scan_and_bag_files/training_set_bag_files/training/t002/drive_2019-03-02-13-12-27.bag";
  bag.open(source_bag);  // BagMode is Read by default

  for(rosbag::MessageInstance const m: rosbag::View(bag))
  {
    sensor_msgs::LaserScan::ConstPtr msg = m.instantiate<sensor_msgs::LaserScan>();
    if (msg != NULL){
      populate_data(msg,0);
    }
  }

  // float t002_1 = all_scans.size() - (above_cellar + cellar_1 + cellar_2 + cellar_3 + corridor + corridor_upper + opposite_corridor);
  float t002_1 = all_scans.size() - (corridor + corridor_upper + opposite_corridor);
  ROS_INFO_STREAM("t002_1: " << t002_1);

  bag.close();


  ///////////////////////////// 9th bag read t002//////////////////////////////////////////////
  source_bag = "/home/nikos/ml_proj/scan_and_bag_files/training_set_bag_files/training/t002/drive_2019-03-02-13-35-36.bag";
  bag.open(source_bag);  // BagMode is Read by default

  for(rosbag::MessageInstance const m: rosbag::View(bag))
  {
    sensor_msgs::LaserScan::ConstPtr msg = m.instantiate<sensor_msgs::LaserScan>();
    if (msg != NULL){
      populate_data(msg,0);
    }
  }

  // float t002_2 = all_scans.size() - (above_cellar + cellar_1 + cellar_2 + cellar_3 + corridor + corridor_upper + opposite_corridor + t002_1);
  float t002_2 = all_scans.size() - (corridor + corridor_upper + opposite_corridor + t002_1);
  ROS_INFO_STREAM("t002_2: " << t002_2);

  bag.close();
  

  ///////////////////////////// 10th bag read t004//////////////////////////////////////////////
  source_bag = "/home/nikos/ml_proj/scan_and_bag_files/training_set_bag_files/training/t004/drive_2019-03-09-10-15-50.bag";
  bag.open(source_bag);  // BagMode is Read by default

  for(rosbag::MessageInstance const m: rosbag::View(bag))
  {
    sensor_msgs::LaserScan::ConstPtr msg = m.instantiate<sensor_msgs::LaserScan>();
    if (msg != NULL){
      populate_data(msg,0);
    }
  }

  // float t004 = all_scans.size() - (above_cellar + cellar_1 + cellar_2 + cellar_3 + corridor + corridor_upper + opposite_corridor + t002_1 + t002_2);
  float t004 = all_scans.size() - (corridor + corridor_upper + opposite_corridor + t002_1 + t002_2);
  ROS_INFO_STREAM("t004: " << t004);
  

  bag.close();


  // ///////////////////////////// Validation set cellar //////////////////////////////////////////////
  // source_bag = "/home/nikos/ml_proj/scan_and_bag_files/training_set_bag_files/validation/cellar/drive_2019-03-04-09-55-13.bag";
  // bag.open(source_bag);  // BagMode is Read by default

  // for(rosbag::MessageInstance const m: rosbag::View(bag))
  // {
  //   sensor_msgs::LaserScan::ConstPtr msg = m.instantiate<sensor_msgs::LaserScan>();
  //   if (msg != NULL){
  //     populate_data(msg,1);
  //   }
  // }

  // float val_cellar = val_set.size();
  // ROS_INFO_STREAM("val_cellar: " << val_cellar);
  

  // bag.close();

  ///////////////////////////// Validation set corridor //////////////////////////////////////////////
  source_bag = "/home/nikos/ml_proj/scan_and_bag_files/training_set_bag_files/validation/corridor/drive_2019-03-04-09-51-06.bag";
  bag.open(source_bag);  // BagMode is Read by default

  for(rosbag::MessageInstance const m: rosbag::View(bag))
  {
    sensor_msgs::LaserScan::ConstPtr msg = m.instantiate<sensor_msgs::LaserScan>();
    if (msg != NULL){
      populate_data(msg,1);
    }
  }

  // float val_corridor = val_set.size() - val_cellar;
  float val_corridor = val_set.size();
  ROS_INFO_STREAM("val_corridor: " << val_corridor);
  

  bag.close();


   ///////////////////////////// Validation set lab //////////////////////////////////////////////
  source_bag = "/home/nikos/ml_proj/scan_and_bag_files/training_set_bag_files/validation/t002/drive_2019-03-04-09-46-27.bag";
  bag.open(source_bag);  // BagMode is Read by default

  for(rosbag::MessageInstance const m: rosbag::View(bag))
  {
    sensor_msgs::LaserScan::ConstPtr msg = m.instantiate<sensor_msgs::LaserScan>();
    if (msg != NULL){
      populate_data(msg,1);
    }
  }

  float val_t002 = val_set.size() - val_corridor;
  ROS_INFO_STREAM("val_t002: " << val_t002);
  

  bag.close();


  ////////////////// Fill the labels ///////////////////////
  ///// Training Labels//////////////////
  // for (int i=0; i<(above_cellar + cellar_1 + cellar_2 + cellar_3); i++){
  //   labels.push_back(0);
  // }

  for (int i=0; i<(corridor + corridor_upper + opposite_corridor); i++){
    labels.push_back(0);
  }

  for (int i=0; i<(t002_1 + t002_2 + t004); i++){
    labels.push_back(1);
  }

  /// Validation labels ///////////////////////
  // for (int i=0; i < val_cellar; i++){
  //   val_labels.push_back(0);
  // }

  for (int i=0; i < val_corridor; i++){
    val_labels.push_back(0);
  }

  for (int i=0; i < val_t002; i++){
    val_labels.push_back(1);
  }

  ///////////////////// Statistics ///////////////////
  ROS_INFO_STREAM("total scans: " << all_scans.size());
  ROS_INFO_STREAM("total labels: " << labels.size());
  ROS_INFO_STREAM("total validation set: " << val_set.size());
  ROS_INFO_STREAM("total validation labels: " << val_labels.size());

  ROS_INFO_STREAM("");
  // float class_cellar_percentage = (above_cellar + cellar_1 + cellar_2 + cellar_3)/all_scans.size();
  float class_corridor_percentage = (corridor + corridor_upper + opposite_corridor)/all_scans.size();
  float class_lab_percentage = (t002_1 + t002_2 + t004)/all_scans.size();
  // ROS_INFO_STREAM("class_cellar_percentage: " << class_cellar_percentage);
  ROS_INFO_STREAM("class_corridor_percentage: " << class_corridor_percentage);
  ROS_INFO_STREAM("class_lab_percentage: " << class_lab_percentage);



  ////////////// dlib training here ///////////////////////
  typedef dlib::linear_kernel<one_scan> lin_kernel;

  // Define the SVM multiclass trainer
  typedef dlib::svm_multiclass_linear_trainer <lin_kernel, float> svm_mc_trainer;
  

  svm_mc_trainer trainer;
  trainer.set_c(0.0002);
  // Train and obtain the decision rule
  dlib::multiclass_linear_decision_function<lin_kernel, float> df = trainer.train(all_scans,labels);
  dlib::serialize("saved_function.dat") << df;

  // randomize_samples(all_scans, labels);
  std::cout << "cross validation: \n" << cross_validate_multiclass_trainer(trainer, val_set, val_labels, 5) << std::endl;
  
  return 0;
}

void populate_data(sensor_msgs::LaserScan::ConstPtr msg, int flag){

  int c = 0;

  for (size_t idx = 0; idx < msg->ranges.size(); idx++)
  {
    
    if (msg->ranges[idx] < msg->range_min)
    {
      if(c<=6){
        if (idx == 0){
          // distances.push_back(msg->range_max);
          one_scan_data(idx, 0) = msg->range_max;
          c = c + 1;
        }
        else{
           one_scan_data(idx, 0) = msg->ranges[idx-1];
          c = c + 1;
        }
      }
      else if(c>6){
         one_scan_data(idx, 0) = msg->range_max;
      }

    }
    else
    {
      one_scan_data(idx, 0) = msg->ranges[idx];
      c = 0;
    }
  }
  if (flag==1){
    val_set.push_back(one_scan_data);
  }
  else{
    all_scans.push_back(one_scan_data);
  }
  
  // ROS_INFO_STREAM( all_scans.size());
}

/////////////////////////////////////////////////////////////////
  // std::vector<float> test;
  // typedef dlib::matrix<float, 720, 1> mymatrix;

  // mymatrix mat;

  // std::vector<mymatrix> samples;

  // // std::vector<sample_type> samples;
  // // std::vector<string> labels;

  // std::ifstream source;                    // build a read-Stream
  // source.open("/home/nikos/ml_proj/scans_to_file_for_ml_corridor2.txt", std::ios_base::in);  // open data
  
  // float x;
 
  // while(source >> x){
  //   test.push_back(x);
  // }
  // for (int i=0;i<1075;i++){
  //   for (int j=0;j<720;j++){
  //     mat(j, 0) = test[i*j+j];
  //     samples.push_back(mat);
  //   }
  // }

  // labels.push_back(label); 

  // typedef linear_kernel<sample_type> lin_kernel;

  // // Define the SVM multiclass trainer
  // typedef svm_multiclass_linear_trainer <lin_kernel, string> svm_mc_trainer;
  // svm_mc_trainer trainer;
  // // Train and obtain the decision rule
  // multiclass_linear_decision_function<lin_kernel,string> df = trainer.train(samples,labels);



// dlib::multiclass_linear_decision_function<lin_kernel, float> df;
//   dlib::deserialize("saved_function.dat") >> df;
//   df = trainer.train(all_scans,labels);