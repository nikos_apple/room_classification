#include <ros/ros.h>
#include <iostream>
#include <fstream>
#include <iterator>
#include <vector>
#include "sensor_msgs/LaserScan.h"

class scan_to_file
{
public:
  ros::NodeHandle nh;
  ros::Subscriber sensor_sub;

  std::vector<float> distances;
  std::vector<float> angles;
  std::ofstream myscans;
    

  scan_to_file()
  {
    sensor_sub = nh.subscribe("/scan", 1000, &scan_to_file::sensorCallback, this);
    myscans.open("scans_to_file_for_ml_t002.txt");
  }


  void sensorCallback(const sensor_msgs::LaserScan::ConstPtr &msg)
  {

    float angle_min = msg->angle_min;
    float angle_max = msg->angle_max;

    // Get the angular distance between each scan point [rad]
    float increment = msg->angle_increment;

    // Clear data from previous scan reading
    distances.clear();
    angles.clear();

    // Store scan angular and distance readings in class-scope vectors
    float angle = angle_min;
    int c = 0;

    for (size_t idx = 0; idx < msg->ranges.size(); idx++)
    {
      
      if (msg->ranges[idx] < msg->range_min)
      {
        if(c<=6){
          if (idx == 0){
            distances.push_back(msg->range_max);
            c = c + 1;
          }
          else{
            distances.push_back(distances[idx-1]);
            c = c + 1;
          }
        }
        else if(c>6){
          distances.push_back(msg->range_max);
        }
        
      }
      else
      {
        distances.push_back(msg->ranges[idx]);
        c = 0;
      }
      angles.push_back(angle);
      angle += increment;

      myscans << distances[idx];
      myscans << " ";
      
    }
    myscans << " \r\n";
  } 
    bool ok()
    {
      return nh.ok();
    }
  
};


int main(int argc, char *argv[])
{
  // Init the connection with the ROS system
  ros::init(argc, argv, "probab_lab");

  // Create class instances
  scan_to_file scan_csv;

  ros::Rate loop_rate(10);
  int i = 0;
  while (scan_csv.ok())
  {
    ros::spinOnce();
    loop_rate.sleep();
  }
  scan_csv.myscans.close();
  return 0;
}